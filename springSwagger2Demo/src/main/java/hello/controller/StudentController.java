package hello.controller;


import hello.entity.Student;
import hello.service.StudentService;
import hello.vo.Mess;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "学生资源管理器")
@RestController
@RequestMapping("/students")
public class StudentController {
	
	@Autowired
	private StudentService studentService;

	@ApiOperation(value = "查询学生列表")
	@GetMapping("")
	public Object getStudents() {
        List<Student> students = studentService.selectList(null);
		return Mess.succ(students);
	}

    @ApiOperation(value = "新建学生记录")
	@PostMapping("")
	public Object postStudent(@RequestBody Student student) {
		studentService.insert(student);
		return Mess.succ(null);
	}

	@GetMapping("/{id}")
    @ApiImplicitParam(name = "id", value = "学生id")
	public Object getStudent(@PathVariable("id") Long id) {
        Student student = studentService.selectById(id);
		return Mess.succ(student);
	}

	@PutMapping("/{id}")
	public Object putStudent(@PathVariable("id") Long id, @RequestBody Student student) {
		Student stu = studentService.selectById(id);
		stu.setAge(student.getAge());
		stu.setName(student.getName());
		stu.setSex(student.getSex());
		studentService.updateById(stu);

		return Mess.succ(null);
	}

	@DeleteMapping("/{id}")
	public Object deleteStudent(@PathVariable Long id) {
	    studentService.deleteById(id);
	    return Mess.succ(null);
    }
}

